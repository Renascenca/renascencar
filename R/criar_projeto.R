criar_projeto <- function(name="analise",
                          title = "A minha análise",
                          folder = getwd(),
                          packagedeps = "packrat",
                          reset = FALSE,
                          open = FALSE,
                          defaultRmd = TRUE,
                          brand = "renascenca") {
    if (!is.character(name)) stop("✋nome tem que ser em texto")
    if (nchar(name) < 2) stop("✋nome precisa de ter pelo menos dois caracteres")

    # check if the requested package dependency is accepted
    #packagedeps <- match.arg(packagedeps, okpackagedeps())

    # for later resetting the project
    #current <- get_current_proj()

    tryCatch({

        #criar projecto
        usethis::create_project(file.path(folder, name),
                                # don't open yet regardless of argument
                                open = FALSE,
                                # use R Studio if it's available
                                rstudio = rstudioapi::isAvailable())


        # set the current project to the new one
        usethis::proj_set(file.path(folder, name),
                          force = TRUE)

        # add DESCRIPTION file and full title
        usethis::use_description()
        desc::desc_set("Title", title,
                       file = usethis::proj_get())

        # setup system for dependencies management
        #setup_dep_system(packagedeps)

        # add new directories
        create_directories(dirs)


    })


}
