
<!-- README.md é gerado a a partir de README.Rmd. Editar esse ficheiro -->

# renascencaR <img src="img/renascencaR_high.png" width="240px" align="right" />

[![lifecycle](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)

`renascencaR` é um package de R inspirado no
[`puddingR`](https://github.com/the-pudding/puddingR) que procura
agregar um conjunto de funções para facilitar a análise de dados para
fins jornalísticos com base nas rotinas de trabalho da equipa de dados
da **Renascença**.

Este pacote está ainda numa fase muito preliminar do seu
desenvolvimento.

À medida que o desenvolvimento for acontecendo, acrescentaremos a
documentação necessária para cada uma das funções.

## Objetivos

  - [x] Produzir um template RMarkdown para uniformizar documentos de
    análise.
  - [ ] Escrever uma função que crie uma estrutura de ficheiros uniforme
    para todos os projetos.
  - [ ] Escrever uma função para gerar codebook para *datasets* a serem
    publicados no Portal de Dados Abertos.

## Instalação

**Não se recomenda a instalação deste package numa fase tão inicial do
seu desenvolvimento\!**

Para obter a versão em desenvolvimento do `renascencaR`.

``` r
## instalar package remotes se não estiver instalar
if (!requireNamespace("remotes", quietly = TRUE)) {
  install.packages("remotes")
}
## instalar versão em desenvolvimento
remotes::install_gitlab("Renascenca/renascencaR")
## Importar package
library(renascencaR)
```
